#ifndef KSQUARES_H
#define KSQUARES_H

#ifdef __cplsuplus
extern "C" {
#endif

#include <stdio.h>
#include <stdbool.h>

typedef struct LinkNode{
        int val;
        struct LinkNode *next;
} LinkNode;

typedef struct Box {
        LinkNode *left;
        LinkNode *up;
        LinkNode *right;
        LinkNode *down;
        const char *owner;
} Box;

typedef enum { 
    LEFT, 
    RIGHT, 
    UP, 
    DOWN, 

    LRUD // Iterate over direction
} Dir;

int Box_setitem(Box *self, Dir direction, bool value);
bool Box_getitem(Box *self, Dir direction);

typedef struct Board {
        int n_rows;
        int n_cols;
        Box **data;
        LinkNode *head;

} Board;


char *Board_string(Board *self);

int Board_init(Board *self, int n_rows, int n_cols);
int Board_populate(Board *self, FILE *fp);
void Board_destroy(Board *self);


typedef struct Player {
        const char *str;
        bool is_human;
        int score;
} Player;


typedef struct Game {
        Board board;
        int turn;
        int n_players;
        Player *players; 
        
} Game;

int Game_init(Game *self, int n_row, int n_cols, int n_players);
int Game_make_move(Game *self, int n_row, int n_col, Dir direction);
int Game_is_active(Game *self);


int Game_get_computer_move(Game *self, int move[3]);
int Game_evaluate_move(Game *self, int row, int col, Dir direction);

#ifdef __cplusplus
}
#endif
#endif
