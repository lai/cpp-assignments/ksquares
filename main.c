#include <stdlib.h>
#include "ksquares.h"

int char_to_dir(char c)
{
    switch (c) {
        case 'u': return UP;
        case 'd': return DOWN;
        case 'l': return LEFT;
        case 'r': return RIGHT;
    }
    return -1;
}

int input(char *array)
{
    int i = 0;
    char c;
    while ((c = fgetc(stdin)) != '\n')
        array[i++] = c;
    return i;

}

int main()
{
   
    Game game;
    Game_init(&game, 3, 3, 2);
        
    game.players[0].str = "K";
    game.players[1].str = "C";

   
    while (Game_is_active(&game)) {
        

        char *str = Board_string(&game.board);
        puts(str);
        free(str);

        printf("Player %s Move\n", game.players[game.turn].str);
        
        if (game.turn == 0) {

            int row, col;
            int result;
            do {
                char buffer[128]; memset(buffer, 0, 128);    
                puts("Enter Row Col: ");

                input(buffer);
                result = sscanf(buffer, "%d %d\n", &row, &col);
            } while (result != 2);
            
            int direction;
            
            do {
                char c;
                char buffer[128]; memset(buffer, 0, 128);
                puts("Enter Direction (L R U D)");
                input(buffer);
                
                result = sscanf(buffer, "%c", &c);
                direction = char_to_dir(c);
            } while (result != 1 || direction < 0);

            Game_make_move(&game, row, col, direction); 

        }
        else {
            int move[3];
            Game_get_computer_move(&game,move);
            Game_make_move(&game, move[0], move[1], move[2]); 
        }
        
    }
}
